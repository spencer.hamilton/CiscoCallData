package com.amazonaws.lambda.cdr;

public class Call 
{
	public String globalCallId;
	public String origLegCallIdentifier;
	public String dateTimeOrigination;
	public String origIpAddr;
	public String callingPartyNumber;
	public String origMediaTransportAddress_IP;
	public String destLegIdentifier;
	public String destNodeId;
	public String originalCalledPartyNumber;
	public String finalCalledPartyNumber;
	public String destCause_value;
	public String dateTimeConnect;
	public String dateTimeDisconnect;
	public String lastRedirectDn;
	public String pkid;
	public String duration;
	public String origDeviceName;
	public String destDeviceName;
	public String origCallTerminationOnBehalfOf;
	public String destCallTerminationOnBehalfOf;
	public String origIpv4v6Addr;
	public String destIpv4v6Addr;
	Call()
	{
		globalCallId = "";
		origLegCallIdentifier = "";
		dateTimeOrigination = "";
		origIpAddr = "";
		callingPartyNumber = "";
		origMediaTransportAddress_IP = "";
		destLegIdentifier = "";
		destNodeId = "";
		originalCalledPartyNumber = "";
		finalCalledPartyNumber = "";
		destCause_value = "";
		dateTimeConnect = "";
		dateTimeDisconnect = "";
		lastRedirectDn = "";
		pkid = "";
		duration = "";
		origDeviceName = "";
		destDeviceName = "";
		origCallTerminationOnBehalfOf = "";
		destCallTerminationOnBehalfOf = "";
		origIpv4v6Addr = "";
		destIpv4v6Addr = "";
		
	}
}
