package com.amazonaws.lambda.cdr;
import java.io.File;

public class FolderScraping 
{
	public void startFileScraping() 
	{
		String directoryPath = "C:\\Users\\spencer.hamilton\\Desktop\\CDR";
		//String directoryPath = System.getenv("directoryPath");
		try
		{
			File directory = new File(directoryPath);
			String[] contents = directory.list();
			String[] fileLocations = new String[contents.length];
			for(int i=0; i<contents.length; i++)
			{
				String fileLocation = directoryPath + "\\" + contents[i];
				fileLocations[i] = fileLocation;
			}
			sendToParse(fileLocations);
			System.out.println("Files successfully transferred!");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	public static void sendToParse(String[] fileLocations)
	{
		ReadFile rf = new ReadFile();
		for(int j=0; j < fileLocations.length; j++)
		{
			String filePath = fileLocations[j];
			rf.readFile(filePath);
			File f = new File(filePath);
			changeFileDirectory(f);
		}
	}
	public static void changeFileDirectory(File f)
	{
		//String newDirectory = System.getenv("newDirectory");
		String newDirectory = "C:\\Users\\spencer.hamilton\\Desktop\\CDR_Read\\";
		f.renameTo(new File(newDirectory + f.getName()));
	}
}
