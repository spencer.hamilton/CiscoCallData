package com.amazonaws.lambda.cdr;
import java.sql.*;
public class MySQLConnect 
{
	public void sendToDB(Call c)
	{
		try
		{
			Connection con = getRemoteConnection();
			Statement stmt = con.createStatement();
			String insertData = " VALUES ("+ c.globalCallId + ","+ c.origLegCallIdentifier+ ","+c.dateTimeOrigination+ ","+c.origIpAddr+ ",'"+c.callingPartyNumber+ "',"+c.origMediaTransportAddress_IP+ ","+c.destLegIdentifier+ ","+c.destNodeId+ ",'"+c.originalCalledPartyNumber+ "','"+c.finalCalledPartyNumber+ "',"+c.destCause_value+ ","+c.dateTimeConnect+ ","+c.dateTimeDisconnect+ ",'"+c.lastRedirectDn+ "','"+c.pkid+ "',"+c.duration+ ",'"+c.origDeviceName+ "','"+c.destDeviceName+ "',"+c.origCallTerminationOnBehalfOf+ ","+c.destCallTerminationOnBehalfOf+ ",'"+c.origIpv4v6Addr+ "','"+c.destIpv4v6Addr +"');";
			String dataColumns = "(CallID, OriginatingLegCallID,CallDateTime,OriginatingIP,CallingPartyNumber,OriginatingTransportID,TerminatingLegID,DestinationNodeID,OriginalPartyNumber,FinalPartyNumber,DestinationClearanceCode,ConnectTime,DisconnectTime,LastRedirectNumber,pkid,Duration,OriginatingDeviceName,DestinationDeviceName,OriginatingTerminationCode,DestinationTerminationCode,OriginatingIPv4v6,DestinationIPv4v6)";
			stmt.executeUpdate("INSERT INTO Cisco_CDR" + dataColumns + insertData);
			con.close();
		}
		catch( Exception e){ System.out.println(e);}
	}
	private static Connection getRemoteConnection()
	{

			try
			{
				String dbName = "CARIBOU"; 
				String userName = "udigger"; 
				String password = "DoWhatYouDig"; 
				String hostname = "cisco-analytics-terminal.clug8sdjw8ls.us-east-1.rds.amazonaws.com";
				String port = "3306";
				String jdbcUrl = "jdbc:mysql://" + hostname + ":" + port + "/" + dbName;
				Connection con = DriverManager.getConnection(jdbcUrl, userName,password);
				return con;
			}
			catch (SQLException e) { System.out.println(e); return null;}

	}
}
