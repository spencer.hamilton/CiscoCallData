package com.amazonaws.lambda.cdr;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ReadFile 
{
	private static int GLOBALCALLID=2;
	private static int ORIGLEGCALLIDENTIFIER=3;
	private static int DATETIMEORIGINATION=4;
	private static int ORIGIPADDR =7;
	private static int CALLINGPARTYNUMBER=8;
	private static int ORIGMEDIATRANSPORTADDRESS_IP =13;
	private static int DESTLEGIDENTIFIER =25;
	private static int DESTNODEID =26;
	private static int ORIGINALCALLEDPARTYNUMBER =29;
	private static int FINALCALLEDPARTYNUMBER =30;
	private static int DESTCAUSE_VALUE =33;
	private static int DATETIMECONNECT =47;
	private static int DATETIMEDISCONNECT =48;
	private static int LASTREDIRECTDN =49;
	private static int PKID =50;
	private static int DURATION =55;
	private static int ORIGDEVICENAME =56;
	private static int DESTDEVICENAME =57;
	private static int ORIGCALLTERMINATIONONBEHALFOF =58;
	private static int DESTCALLTERMINATIONONBEHALFOF =59;
	private static int ORIGIPV4V6ADDR =80;
	private static int DESTIPV4V6ADDR =81;
	public void readFile(String fileLocation)
	{
		BufferedReader br = null;
		try
		{
			br = new BufferedReader(new FileReader(fileLocation));
			
			String contentLine = br.readLine();
			String[] stringArray = new String[4];
			for ( int i=0; contentLine != null; i++ )
			{
				stringArray[i] = contentLine;
				contentLine = br.readLine();
			}
			String dataName = stringArray[0];
			dataName = dataName.replaceAll("\"", "");
			String data = stringArray[2];
			data = data.replaceAll("\"", "");
			
			String[] origCategories = dataName.split(",");
			String[] Values = data.split(",");
			String[] Categories = new String[origCategories.length-1];
			for(int h=0; h < Categories.length ; h++)
			{
				Categories[h] = origCategories[h+1];
			}
			Call c = new Call();
			c.globalCallId = Values[GLOBALCALLID];
			c.origLegCallIdentifier = Values[ORIGLEGCALLIDENTIFIER];
			c.dateTimeOrigination = Values[DATETIMEORIGINATION];
			c.origIpAddr = Values[ORIGIPADDR];
			c.callingPartyNumber = Values[CALLINGPARTYNUMBER];
			c.origMediaTransportAddress_IP = Values[ORIGMEDIATRANSPORTADDRESS_IP];
			c.destLegIdentifier = Values[DESTLEGIDENTIFIER];
			c.destNodeId = Values[DESTNODEID];
			c.originalCalledPartyNumber = Values[ORIGINALCALLEDPARTYNUMBER];
			c.finalCalledPartyNumber = Values[FINALCALLEDPARTYNUMBER];
			c.destCause_value = Values[DESTCAUSE_VALUE];
			c.dateTimeConnect = Values[DATETIMECONNECT];
			c.dateTimeDisconnect = Values[DATETIMEDISCONNECT];
			c.lastRedirectDn = Values[LASTREDIRECTDN];
			c.pkid = Values[PKID];
			c.duration = Values[DURATION];
			c.origDeviceName = Values[ORIGDEVICENAME];
			c.destDeviceName = Values[DESTDEVICENAME];
			c.origCallTerminationOnBehalfOf = Values[ORIGCALLTERMINATIONONBEHALFOF];
			c.destCallTerminationOnBehalfOf = Values[DESTCALLTERMINATIONONBEHALFOF];
			c.origIpv4v6Addr = Values[ORIGIPV4V6ADDR];
			c.destIpv4v6Addr = Values[DESTIPV4V6ADDR];

			MySQLConnect con = new MySQLConnect();
			con.sendToDB(c);
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
		finally 
		{
			try
			{
				if(br != null)
					br.close();
			}
			catch( IOException ioe)
			{
				System.out.println("Error in closing the BufferedReader");
			}
		}
	}
}